import datetime
from typing import Optional

trip_duration = datetime.timedelta(minutes=2)
weekdays = [
    {
        "start": datetime.timedelta(hours=7, minutes=30),
        "end": datetime.timedelta(hours=24, minutes=30),
        "frequency": datetime.timedelta(minutes=10),
    }
]
saturdays = [
    {
        "start": datetime.timedelta(hours=7, minutes=30),
        "end": datetime.timedelta(hours=12, minutes=0),
        "frequency": datetime.timedelta(minutes=10),
    },
    {
        "start": datetime.timedelta(hours=12, minutes=0),
        "end": datetime.timedelta(hours=18, minutes=0),
        "frequency": datetime.timedelta(minutes=5),
    },
    {
        "start": datetime.timedelta(hours=18, minutes=0),
        "end": datetime.timedelta(hours=0, minutes=30),
        "frequency": datetime.timedelta(minutes=10),
    },
]
sundays = [
    {
        "start": datetime.timedelta(hours=9, minutes=0),
        "end": datetime.timedelta(hours=23, minutes=0),
        "frequency": datetime.timedelta(minutes=10),
    }
]


def get_frequency(time: datetime.datetime) -> Optional[datetime.timedelta]:
    weekday = time.weekday()  # 0 to 6, 0 is Monday
    if weekday <= 4:
        schedule = weekdays
    elif weekday == 5:
        schedule = saturdays
    else:
        schedule = sundays

    midnight = time.replace(hour=0, minute=0, second=0, microsecond=0)
    for interval in schedule:
        start = midnight + interval["start"]
        end = midnight + interval["end"]

        if start <= time <= end:
            return interval["frequency"]

    return None


def is_open(time: datetime.datetime) -> bool:
    return get_frequency(time) is not None


def is_active(time: datetime.datetime) -> bool:
    freq = get_frequency(time)
    if freq is not None:
        start = time.replace(minute=0, second=0, microsecond=0)
        while start <= time:
            end = start + trip_duration
            if start <= time <= end:
                return True
            start += freq
    return False
