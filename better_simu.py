import tkinter as tk
import requests

base_url = "https://applications002.brest-metropole.fr/WIPOD01/Transport/REST/getStopVehiclesPosition?format=json&route_id=A&trip_headsign={trip_headsign}"

trip_headsigns = [
    t["Trip_headsign"]
    for t in requests.get(
        "https://applications002.brest-metropole.fr/WIPOD01/Transport/REST/getDestinations?format=json&route_id=A"
    ).json()
]

arrets = {
    "PTGOU_TR": "Porte de Gouesnou",
    "KERGA_TA|KERGA_TR": "Kergaradec",
    "MESME_TA|MESME_TR": "Mesmerrien",
    "PTGUI_TA": "Porte Guipavas",
    "KERLA_TA|KERLA_TR": "Kerlaurent",
    "EAU_B_TA|EAU_B_TR": "Eau Blanche",
    "PONTA_TA|PONTA_TR": "Pontanézen",
    "EUROP_TA|EUROP_TR": "Europe",
    "MENEZ_TA|MENEZ_TR": "Menez Paul",
    "PLSTR_TA|PLSTR_TR": "Place Strasbourg",
    "PILIE_TR|PILIE_TA": "Pilier Rouge",
    "OCTRO_TA|OCTRO_TR": "Octroi",
    "SMART_TA|SMART_TR": "Saint-Martin",
    "JJAUR_TA|JJAUR_TR": "Jean Jaurès",
    "LIBE_TA|LIBE_TR": "Liberté",
    "SIAM_TA|SIAM_TR": "Siam",
    "CHAT_TA|CHAT_TR": "Château",
    "RECOU_TA|RECOU_TR": "Recouvrance",
    "MAC_O_TA|MAC_O_TR": "Mac Orlan",
    "ST_EX_TA|ST_EX_TR": "Saint-Exupéry",
    "CAPUC_TA|CAPUC_TR": "Les Capucins",
    "DUPUY_TA|DUPUY_TR": "Dupuy de Lôme",
    "POLYG_TA|POLYG_TR": "Polygone",
    "VALI_TA|VALI_TR": "Vali Hir",
    "COAT_TA|COAT_TR": "Coat Tan",
    "KROUX_TR|KROUX_TA": "Keranroux",
    "FMONT_TA|FMONT_TR": "Fort Montbarey",
    "PTPLZ_TA|PTPLZ_TR": "Porte Plouzané",
}


class Station:
    def __init__(self, key: str, name: str, app: "Application"):
        self.key = key
        self.name = name

        self.app = app
        self.label = tk.Label(app, text=name)
        self.label.pack(anchor="nw")
        self.default_bg = self.label.cget("background")

    def update_status(self):
        fon = bon = False
        for trip_headsign, stops in self.app.active.items():
            for stop in stops:
                if stop["Stop_id"] in self.key:
                    if "G" in trip_headsign:
                        bon = True
                    else:
                        fon = True

        if fon and bon:
            self.label.config(bg="yellow")
            self.label["text"] = f"{self.name} ↓↑"
        elif fon:
            self.label.config(bg="red")
            self.label["text"] = f"{self.name} ↓"
        elif bon:
            self.label.config(bg="green")
            self.label["text"] = f"{self.name} ↑"
        else:
            self.label.config(bg=self.default_bg)
            self.label["text"] = self.name


class Application(tk.Frame):
    def __init__(self, master: tk.Tk) -> None:
        super().__init__(master)

        self.master = master
        self.arrets: dict[str, Station] = {}
        self.active: dict[str, list] = {}
        self.last_updated = 0

        self.master.title("Simu PCB Brest")
        self.pack()
        self.create_widgets()

    def create_widgets(self) -> None:
        for pk, name in arrets.items():
            self.arrets[pk] = Station(pk, name, self)

    def update_status(self):
        trip_headsign = trip_headsigns[self.last_updated]
        self.last_updated = (self.last_updated + 1) % len(trip_headsigns)

        res = requests.get(base_url.format(trip_headsign=trip_headsign))
        if res.status_code == 200:
            stops = res.json()
            print(trip_headsign, stops)
            self.active[trip_headsign] = stops
        else:
            print(res, res.text)

        for station in self.arrets.values():
            station.update_status()

        self.after(1500, self.update_status)


if __name__ == "__main__":
    root = tk.Tk()
    app = Application(root)
    app.after(1500, app.update_status)
    app.mainloop()
