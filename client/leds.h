#include "params.h"

void leds_setup() {
    pinMode(OE_, OUTPUT);
    pinMode(SRCLK, OUTPUT);
    pinMode(RCLK, OUTPUT);
    pinMode(SRCLR_, OUTPUT);
    pinMode(SER, OUTPUT);
}

void leds_clear() {
    digitalWrite(OE_, HIGH);
    digitalWrite(SRCLR_, LOW);
    delay(FREQ);
    digitalWrite(RCLK, HIGH);
    delay(FREQ);
    digitalWrite(RCLK, LOW);
    delay(FREQ);
    digitalWrite(SRCLR_, HIGH);
    digitalWrite(OE_, LOW);
}

void leds_send_byte(uint8_t val) {
    for (int i=0; i<8; i++) {
        digitalWrite(SER, (val>>7) & 1);
        delay(FREQ);
        digitalWrite(SRCLK, HIGH);
        delay(FREQ);
        digitalWrite(SRCLK, LOW);
        delay(FREQ);
        val = val << 1;
    }
}

void leds_send(uint32_t val) {
    uint8_t llsb = val;
    val = val >> 8;
    uint8_t lsb = val;
    val = val >> 8;
    uint8_t msb = val;
    val = val >> 8;
    uint8_t mmsb = val;

    leds_send_byte(mmsb);
    leds_send_byte(msb);
    leds_send_byte(lsb);
    leds_send_byte(llsb);
}

void leds_flush() {
    digitalWrite(RCLK, HIGH);
    delay(FREQ);
    digitalWrite(RCLK, LOW);
}

void leds_animate(uint32_t* animation, int delay_) {
  int i = 0;
  while (animation[i] != 0) {
    leds_send(animation[i]);
    leds_flush();
    delay(delay_);
    i++;
  }
}
