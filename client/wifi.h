#include <ESP8266WiFi.h>

const char* ssid     = MY_SSID;
const char* password = MY_PWD;
const char* host = MY_HOST;
const char* hostIP = MY_HOSTIP;
String url = "/";

void connectWifi(const char* ssid, const char* password) {
    int WiFiCounter = 0;
    // We start by connecting to a WiFi network
    PRINTDEBUG("Connecting to ");
    PRINTDEBUG(ssid);
    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED && WiFiCounter < 30) {
        delay(1000);
        WiFiCounter++;
        PRINTDEBUG(".");
    }

    if (WiFi.status() != WL_CONNECTED) {
        PRINTDEBUG("");
        PRINTDEBUG("Failed to connect");
    } else {
        PRINTDEBUG("");
        PRINTDEBUG("WiFi connected");
        PRINTDEBUG("IP address: ");
        PRINTDEBUG(WiFi.localIP());
    }
}

void wifi_setup() {
    connectWifi(ssid, password); // Start WiFi
    delay(10);
}

unsigned long whileTimeout = 0;
uint32_t wifi_query() {
    // Use WiFiClient class to create TCP connections
    PRINTDEBUG("connecting to ");
    PRINTDEBUG(host);
    delay(10);
    WiFiClient client;
    const int httpPort = MY_HOSTPORT;
    if (!client.connect(hostIP, httpPort)) {
        PRINTDEBUG("connection failed");
        return 0;
    }

    // This will send the request to the server
    PRINTDEBUG("Requesting URL: ");
    delay(10);
    String timeFromReset = String( (int)((millis() / 1000) / 60), DEC);
    PRINTDEBUG(url);
    delay(10);
    client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                "Host: " + host + "\r\n" +
                "Connection: close\r\n\r\n");
    delay(100);

    int dataFlag = 0;
    int dataFlagCounter = 0;
    uint32_t ret = 0;
    while (!dataFlag) {
        whileTimeout = millis();
        while (client.available() &&  ((millis() - whileTimeout) < 5000) ) {
            if (!dataFlag) dataFlag = 1;
            String line = client.readStringUntil('\r');
            if (int prefix_idx = line.indexOf("LEDS") != -1) {
                PRINTDEBUG(line);
                line = line.substring(prefix_idx+4);
                bool data_left = true;
                while (data_left) {
                    long id = line.toInt();
                    ret |= (1 << (id - 1));
                    PRINTDEBUG(line);
                    PRINTDEBUG(ret);

                    int idx = line.indexOf(',');
                    if (idx == -1) {
                        data_left = false;
                    } else {
                        line = line.substring(idx+1);
                    }
                }
            }
        }
        if  (!dataFlag) {
            dataFlagCounter++;
            if (dataFlagCounter > 20) {
                PRINTDEBUG("Connection Failed!");
                break;
            }
            delay(500);
        }
    }
    client.flush();
    client.stop();

    delay(100);

    PRINTDEBUG();
    PRINTDEBUG("closing connection");

    return ret;
}
