#ifndef PARAMS_H
#define PARAMS_H

#define OE_ 12
#define SRCLK 13
#define RCLK 14
#define SRCLR_ 15
#define SER 16
#define FREQ 1

#define MY_SSID "nils-hotspot"
#define MY_PWD ""
#define MY_HOST "10.42.0.1"
#define MY_HOSTIP MY_HOST
#define MY_HOSTPORT 8080

#define DEBUG 0

#if DEBUG
#define PRINTDEBUG(STR) \
  {  \
    Serial.println(STR); \
  }
#else
#define PRINTDEBUG(STR)
#endif

#endif
