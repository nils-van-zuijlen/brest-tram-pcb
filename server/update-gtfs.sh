#!/bin/sh

curl --output gtfs.zip https://ratpdev-mosaic-prod-bucket-raw.s3-eu-west-1.amazonaws.com/11/exports/1/gtfs.zip
rm gtfs.db
gtfs2db append gtfs.zip gtfs.db
