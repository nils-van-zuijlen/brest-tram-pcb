import asyncio
import json
from typing import Any, Callable, Coroutine, Optional
import random
import aiohttp
from aiohttp import web
from math import radians, degrees, sin, cos, asin, acos, sqrt, inf
import pygtfs
from google.transit import gtfs_realtime_pb2


GTFS_RT_URL = (
    "https://proxy.transport.data.gouv.fr/resource/bibus-brest-gtfs-rt-vehicle-position"
)


def distance(lon1, lat1, lon2, lat2):
    """Great circle distance between points

    https://blog.petehouston.com/calculate-distance-of-two-locations-on-earth/
    """
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    return 6371 * (
        acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lon1 - lon2))
    )


async def feed_to_position(
    feed: gtfs_realtime_pb2.FeedMessage, schedule: pygtfs.Schedule
) -> list:
    positions = []
    for entity in feed.entity:
        vehicle = entity.vehicle
        if vehicle.trip.route_id == "A":
            lon = vehicle.position.longitude
            lat = vehicle.position.latitude

            trip = schedule.trips_by_id(vehicle.trip.trip_id)[0]
            stops = (st.stop for st in trip.stop_times)

            min_dist = inf
            near = None
            for stop in stops:
                if (d := distance(lon, lat, stop.stop_lon, stop.stop_lat)) < min_dist:
                    min_dist = d
                    near = stop

            positions.append(near)
    return positions


async def get_vehicles_position(
    session: aiohttp.ClientSession,
) -> Optional[gtfs_realtime_pb2.FeedMessage]:
    try:
        async with session.get(GTFS_RT_URL) as resp:
            if resp.status == 200:
                feed = gtfs_realtime_pb2.FeedMessage()
                feed.ParseFromString(await resp.read())
                return feed
            elif resp.status == 429:  # Too Many Requests, retry after 2s
                print("TMR get_vehicles_position")
                await asyncio.sleep(2)
                return await get_vehicles_position(session, route_id, trip_headsign)
            else:
                print(resp.status)
                return None
    except Exception as e:
        print(e)
        return None


async def update_positions(app: web.Application) -> None:
    inter_request_delay = 10
    try:
        session = aiohttp.ClientSession()
        schedule = pygtfs.Schedule("gtfs.db")

        while True:
            if (
                feed := await get_vehicles_position(session)
            ) is not None:
                app["vehicle_positions"] = await feed_to_position(feed, schedule)
                print(".", end="", flush=True)
            else:
                print("V", end="", flush=True)
            await asyncio.sleep(inter_request_delay)

    except asyncio.CancelledError:
        pass
    finally:
        await session.close()


async def fake_positions_updater(app: web.Application) -> None:
    period = 2
    try:
        blinky = set()
        realist = set()
        current_stop = next(iter(app["stops"].values()))
        app["faked_positions"]["blinky"] = blinky
        app["faked_positions"]["realist"] = realist
        while True:
            for i in range(1, 32):
                blinky.add(i)

                realist.clear()
                realist.add(current_stop["Led_id"])
                current_stop = app["stops"][random.choice(current_stop["Next_stops"])]
                await asyncio.sleep(period)
            blinky.clear()

            realist.clear()
            realist.add(current_stop["Led_id"])
            current_stop = app["stops"][random.choice(current_stop["Next_stops"])]
            await asyncio.sleep(period)
    except asyncio.CancelledError:
        pass


async def start_background_tasks(app: web.Application) -> None:
    app["vehicle_positions"] = {}
    app["faked_positions"] = {}
    app["positions_updater"] = asyncio.create_task(update_positions(app))
    app["fake_positions_updater"] = asyncio.create_task(fake_positions_updater(app))


async def cleanup_background_tasks(app: web.Application) -> None:
    # app["positions_updater"].cancel()
    # await app["positions_updater"]
    app["fake_positions_updater"].cancel()
    await app["fake_positions_updater"]


async def index(request: web.Request) -> web.Response:
    current_positions = request.app["vehicle_positions"]
    stops = request.app["stops"]

    leds = {
        stops[stop.stop_id]["Led_id"] for stop in current_positions
    }

    return web.Response(text="LEDS" + ",".join(str(l) for l in leds) + "\r")


def filtered(filter: str) -> Callable[[web.Request], Coroutine[Any, Any, web.Response]]:
    async def filtered_(request: web.Request) -> web.Response:
        current_positions = request.app["vehicle_positions"]
        stops = request.app["stops"]

        leds = {
            stops[stop.stop_id]["Led_id"]
            for stop in current_positions
            if filter in stop.stop_id
        }

        return web.Response(text="LEDS" + ",".join(str(l) for l in leds) + "\r")

    return filtered_


async def debug(request: web.Request) -> web.Response:
    current_positions = request.app["vehicle_positions"]
    active_stops = {stop.stop_id for stop in current_positions}

    return web.Response(text="\n".join(active_stops))


def filtered_debug(
    filter: str,
) -> Callable[[web.Request], Coroutine[Any, Any, web.Response]]:
    async def filtered_debug_(request: web.Request) -> web.Response:
        current_positions = request.app["vehicle_positions"]
        active_stops = {
            stop.stop_id for stop in current_positions if filter in stop.stop_id
        }

        return web.Response(text="\n".join(active_stops))

    return filtered_debug_


def faked(kind: str) -> Callable[[web.Request], Coroutine[Any, Any, web.Response]]:
    async def faked_(request: web.Request) -> web.Response:
        return web.Response(
            text="LEDS"
            + ",".join(str(l) for l in request.app["faked_positions"][kind])
            + "\r"
        )

    return faked_


def main() -> None:
    with open("stops.json") as f:
        stops = json.load(f)

    app = web.Application()
    app["stops"] = stops
    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)
    app.add_routes(
        [
            web.get("/", index),
            web.get("/TA", filtered("_TA")),
            web.get("/TR", filtered("_TR")),
            web.get("/fake", faked("blinky")),
            web.get("/fake/realist", faked("realist")),
            web.get("/debug", debug),
            web.get("/debug/TA", filtered_debug("_TA")),
            web.get("/debug/TR", filtered_debug("_TR")),
        ]
    )
    web.run_app(app)


if __name__ == "__main__":
    main()
